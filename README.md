# MonthlyPerformanceReport

This is a Gradle project. 

There is builded jar into <a href="https://gitlab.com/tsr74/monthlyperformancereport/-/tree/master/out/artifacts/MonthlyPerformanceReport_jar">out/artifacts/MonthlyPerformanceReport_jar/</a> directory.

You can start it like this:
**java -jar MonthlyPerformanceReport.jar dataEmployeesFileName definitionFileName monthlyReportFileName**
